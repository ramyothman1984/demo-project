import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Observable, from } from 'rxjs';
import { MainLayaoutComponent } from './components/layout/main-layaout/main-layaout.component';
import { NavigationComponent } from './components/layout/common/navigation/navigation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HotelMapsComponent } from './components/layout/common/hotel-maps/hotel-maps.component';
import { GeoMapsService } from './services/geo-maps.service';
import { HomeComponent } from './containers/home/home.component';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { AlertsComponent } from './components/layout/common/alerts/alerts.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
import * as profileReducer from './reducers/profile';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { DragScrollModule } from 'ngx-drag-scroll';
import { HotelCarousalComponent } from './components/layout/common/hotel-carousal/hotel-carousal.component';
import { ConfirmBookingComponent } from './containers/confirm-booking/confirm-booking.component';
import { HotelService } from './services/hotel.service';
import { AuthService } from './services/auth.service';

export class HashTranslateLoader implements TranslateLoader {
  getTranslation(lang: string): Observable<any> {
    if (lang.indexOf('-')) {
      lang = lang.slice(0, 2);
    }
    return from(import(`../assets/i18n/${lang}.json`))
  }
}

@NgModule({
  declarations: [
    AppComponent,
    MainLayaoutComponent,
    NavigationComponent,
    HotelMapsComponent,
    HomeComponent,
    NotFoundComponent,
    AlertsComponent,
    HotelCarousalComponent,
    ConfirmBookingComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DragScrollModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: HashTranslateLoader
      }
    }),
    // environment.imports,
    StoreDevtoolsModule.instrument({ maxAge: 7 }),
    StoreModule.forRoot(reducers),
    StoreModule.forFeature('profile', profileReducer.reducer),
  ],
  providers: [GeoMapsService, HotelService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
