import { Auth } from 'aws-amplify';

import { Injectable, NgZone } from '@angular/core';
import { GeoLocation } from '../models/geo-location';
import * as fromRoot from '../reducers';
import * as appActions from '../actions/application';
import { Store } from '@ngrx/store';
import {generate} from 'shortid'
declare var google: any;
@Injectable()
export class AuthService {
  constructor(private storeRoot: Store<fromRoot.AppState>) {}

  async signUp(username, email, password) {
      try {
          const user  = await Auth.signUp({
              username,
              password,
              attributes: {
                  email
              }
          });
          return user;
      } catch(error) {
        this.storeRoot.dispatch(new appActions.AddAlertAction( {alert: {id: generate(),
            title: 'Error signing up',
            message: error.message,
            type: 'danger'}}));
        console.log('error signing up: ', error);
      }
  }

  confirmSignUp(username, code) {
      try {
          return Auth.confirmSignUp(username, code);
      } catch(error) {
        this.storeRoot.dispatch(new appActions.AddAlertAction( {alert: {id: generate(),
            title: 'Error confirmation sign up',
            message: error.message,
            type: 'danger'}}));
          console.log('error confirmation sign up', error);
      }
  }

  async resendConfirmationCode(username) {
      try {
          await Auth.resendSignUp(username);
          console.log('code resent successfully');
      } catch(error) {
        this.storeRoot.dispatch(new appActions.AddAlertAction( {alert: {id: generate(),
            title: 'Error resending code',
            message: error.message,
            type: 'danger'}}));
          console.log('error resending code: ', error);
      }
  }

  async signIn(username, password) {
      try {
          return await Auth.signIn(username, password);
      } catch(error) {
        this.storeRoot.dispatch(new appActions.AddAlertAction( {alert: {id: generate(),
            title: 'Error signing in',
            message: error.message,
            type: 'danger'}}));
          console.log('error signing in', error);
      }
  }

  async signOut() {
      try {
          await Auth.signOut();
      } catch(error) {
        this.storeRoot.dispatch(new appActions.AddAlertAction( {alert: {id: generate(),
            title: 'Error signing out',
            message: error.message,
            type: 'danger'}}));
          console.log('error signing out: ', error);
      }
  }
}
