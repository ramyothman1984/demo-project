import { Injectable, NgZone } from '@angular/core';
import { GeoLocation } from '../models/geo-location';
declare var google: any;
@Injectable()
export class GeoMapsService {
  constructor(private ngZone: NgZone) {}
  // default location when no location is provided set to berlin coordinates
  defaultLocation: GeoLocation = {lat: 52.520008, lng: 13.404954};
  initializeMap(location: GeoLocation,mapHtmlElement: any, typeId?: string, zoom?: number): any {
    const mapProp = {
        center: location ? location : this.defaultLocation,
        zoom: zoom ? zoom : 12,
        mapTypeId: typeId ? typeId : google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        zoomControl: false,
        streetViewControl: false,
        fullscreenControl: false
    };
    return new google.maps.Map(mapHtmlElement, mapProp);
  }

  setCenter(mapElement, location: GeoLocation) {
    mapElement.setCenter(new google.maps.LatLng(location.lat, location.lng));
  }

  addListener(mapElement: any, eventType: string, func: any) {
      mapElement.addListener( eventType, (e) => {
        this.ngZone.run(() => func(e))
      });
  }

  addMarker(mapElement: any, latlng: any, title: string, description: string, iconPath: string) {
    console.log('setting marker');
      const marker = new google.maps.Marker({
          position: latlng,
          map: mapElement,
          title: name,
          icon: iconPath
      });

      const contentString = '<div id="content">'+
    '<div id="siteNotice">'+
    '</div>'+
    '<h3 id="firstHeading" class="firstHeading">'+ title +'</h3>'+
    '<div id="bodyContent">'+
    '<hr/>' +
    '<p><b>Description : </b> '+ description
    '</p></div></div>';

    const infoWindow = new google.maps.InfoWindow({
        content: contentString,
        maxWidth: 300
    });

    marker.addListener('click', (e) => {
        infoWindow.open(mapElement, marker);
    });
  }
}
