import { Injectable, NgZone } from '@angular/core';
import { GeoLocation } from '../models/geo-location';
import { HotelInfo } from '../models/hotel';
declare var google: any;
@Injectable()
export class HotelService {
  hotelNames = [
    "Mercure",
    "Lime",
    "Sunrise",
    "Sheraton",
    "Hilton"
  ];
  hotelSurNames = [
    "Sunshine",
    "Home",
    "Appartment",
    "Holidays",
    "Hotel"
  ];
  hotelImages = [
    "https://cf.bstatic.com/xdata/images/hotel/square200/236603653.webp?k=f22b56f8f441c985e030deb2f9ae158d612bf672e07dde2929d00afe7460f9fe&o=",
    "https://cf.bstatic.com/xdata/images/hotel/square200/244901622.webp?k=792bc5dc53157773530e6b879485219b8eb447337b1422e7ef68442a8c48d9dc&o=",
    "https://cf.bstatic.com/xdata/images/hotel/square200/267324236.webp?k=4818147df8b767e58ad2bae86f08f9757a67d8d51aaaab6aba70e79fd42db365&o=",
    "https://cf.bstatic.com/xdata/images/hotel/square200/272425395.webp?k=c78fe74b4d61797f0450b64b14e342aa622b6a6c5dbd6efdb51673fd57551d67&o=",
    "https://cf.bstatic.com/xdata/images/hotel/square200/53969453.webp?k=09e1b715ac3f176c9d310fe3a079f6a37b22668df43aca89961545b33a06a3ae&o="
  ];

  constructor() {}

  generateLocations(geoLocation: GeoLocation): {
    hotelsInfo: HotelInfo[],
    geoLocation: GeoLocation,
    selectedHotel: HotelInfo} {
    //Data setting
    
    let index = -1;
    const selectedIndex = Math.floor(Math.random() * 10) % 5;
    console.log(Math.random());
    const hotels = [];
    let selectedHotel: HotelInfo;
    this.hotelNames.forEach(hotel => {
      index++;
      const name = `${hotel} ${this.hotelSurNames[Math.floor(Math.random() * 10) % 5]}`;
      const info = new HotelInfo();

      info.id = `tid-${index}`;
      info.name = name;
      info.description = `${Math.floor(Math.random() * 10) % 4}.${Math.floor(Math.random() * 10) % 9} KM from the city center`;
      info.photo = this.hotelImages[Math.floor((Math.random() * 10) % 5)];
      const addedLat = parseFloat(`0.0${Math.random() * 100000}`);
      const addedLng = parseFloat(`0.0${Math.random() * 100000}`);
      
      info.location = { lat: geoLocation.lat + addedLat, lng: geoLocation.lng + addedLng };
      if (selectedIndex === index) {
        selectedHotel = { ... info};
      }
      
      hotels.push(info);
    });

    return { hotelsInfo: [... hotels], geoLocation: {... selectedHotel.location}, selectedHotel: selectedHotel} 
  }
  
}
