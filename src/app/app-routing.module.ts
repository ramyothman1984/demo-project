import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './containers/home/home.component';
import { MainLayaoutComponent } from './components/layout/main-layaout/main-layaout.component';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { ConfirmBookingComponent } from './containers/confirm-booking/confirm-booking.component';
const routes: Routes = [
  {
    path: '',
    component: MainLayaoutComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'confirm-booking',
        component: ConfirmBookingComponent
      },
      {
        path: 'not-found',
        component: NotFoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
