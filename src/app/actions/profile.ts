import { Action } from '@ngrx/store';

import { Identity } from '../models/identity';
import { PROFILE_ACTIONS } from './actions-definition';
import { PageData } from '../models/page-data';

export class LoadIdentityAction implements Action {
  readonly type = PROFILE_ACTIONS.LOAD_IDENTITY;

  constructor(public payload?: any) { }
}

export class LoadIdentitySuccessAction implements Action {
  readonly type = PROFILE_ACTIONS.LOAD_IDENTITY_SUCCESS;

  constructor(public payload: {identity: Identity}) { }
}

export class LoadIdentityFailAction implements Action {
  readonly type = PROFILE_ACTIONS.LOAD_IDENTITY_FAIL;

  constructor(public payload?: any) { }
}

export class LoadPageData implements Action {
  readonly type = PROFILE_ACTIONS.LOAD_PAGE_DATA;

  constructor(public payload: { url: string, route: any }) { }
}

export class LoadPageDataComplete implements Action {
  readonly type = PROFILE_ACTIONS.LOAD_PAGE_DATA_COMPLETE;

  constructor(public payload: { page: PageData }) { }
}

export class LoadUserSettingsAction implements Action {
  readonly type = PROFILE_ACTIONS.LOAD_USER_SETTINGS;

  constructor(public payload?: any) { }
}

export type Actions = LoadIdentityAction | LoadIdentitySuccessAction | LoadIdentityFailAction |
  LoadPageData | LoadPageDataComplete | LoadUserSettingsAction;
