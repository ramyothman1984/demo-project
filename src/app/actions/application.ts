import { Action } from '@ngrx/store';

import { Alert } from '../models/alert';
import { APPLICATION_ACTIONS } from './actions-definition';
import { HotelInfo } from '../models/hotel';

export class AddAlertAction implements Action {
  readonly type = APPLICATION_ACTIONS.ADD_ALERT;

  constructor(public payload: { alert: Alert }) { }
}

export class DismissAlertAction implements Action {
  readonly type = APPLICATION_ACTIONS.DISMISS_ALERT;

  constructor(public payload: string) { }
}

export class BookHotelAction implements Action {
  readonly type = APPLICATION_ACTIONS.BOOK_HOTEL;

  constructor(public payload: HotelInfo) { }
}

export type Actions = AddAlertAction | DismissAlertAction | BookHotelAction;

