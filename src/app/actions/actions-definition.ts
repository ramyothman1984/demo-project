export const APPLICATION_ACTIONS = {
    ADD_ALERT: '[Application] Add Alert',
    DISMISS_ALERT: '[Application] Dismiss Alert',
    BOOK_HOTEL: '[Booking Actions] Book Hotel'
};

export const PROFILE_ACTIONS = {
    LOAD_IDENTITY: '[Profile] Load Identity',
    LOAD_IDENTITY_SUCCESS: '[Profile] Identity Loaded',
    LOAD_IDENTITY_FAIL: '[Profile] Identity Load Error',
    LOAD_PAGE_DATA: '[Profile] Load Page Data',
    LOAD_PAGE_DATA_COMPLETE: '[Profile] Load Page Data Complete',
    LOAD_USER_SETTINGS: '[Profile] Load User Settings',
    LOAD_USER_SETTINGS_SUCCESS: '[Profile] Load User Settings Success',
    LOAD_USER_SETTINGS_FAIL: '[Profile] Load User Settings Fail'
};
