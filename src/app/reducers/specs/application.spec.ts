import { reducer } from '../application';
import * as fromApplication from '../application';
import { AddAlertAction, DismissAlertAction } from '../../actions/application';

describe('ApplicationReducer', () => {

  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const result = reducer(undefined, action);
      expect(result).toEqual(fromApplication.initialState);
    });
  });

  describe('ADD_ALERT', () => {
    it('should add new alert without changing loading property', () => {
      const initialState: fromApplication.State = {
        alerts: [],
        bookedHotel: null
      };

      const expectedResult: fromApplication.State = {
        ...initialState,
        alerts: [
          { type: 'danger', id: '111', message: 'foo' }
        ]
      };

      const result = reducer(initialState, new AddAlertAction({
        alert: { type: 'danger', id: '111', message: 'foo' }
      }));
      expect(result).toEqual(expectedResult);
    });

    it('should add new alert and change loading property', () => {
      const initialState: fromApplication.State = {
        alerts: [],
        bookedHotel: null
      };

      const expectedResult: fromApplication.State = {
        alerts: [
          { type: 'danger', id: '111', message: 'foo' }
        ],
        bookedHotel: null
      };

      const result = reducer(initialState, new AddAlertAction({
        alert: { type: 'danger', id: '111', message: 'foo' },
      }));
      expect(result).toEqual(expectedResult);
    });
  });

  describe('DISMISS_ALERT', () => {
    it('should remove an alert by its id', () => {
      const initialState: fromApplication.State = {
        alerts: [
          { type: 'danger', id: '111', message: 'foo' },
          { type: 'danger', id: '222', message: 'foo' }
        ],
        bookedHotel: null
      };

      const expectedResult: fromApplication.State = {
        alerts: [
          { type: 'danger', id: '111', message: 'foo' }
        ],
        bookedHotel: null
      };

      const result = reducer(initialState, new DismissAlertAction('222'));
      expect(result).toEqual(expectedResult);
    });

    it('should leave the state as it is when no alert is found by its id', () => {
      const initialState: fromApplication.State = {
        alerts: [
          { type: 'danger', id: '111', message: 'foo' },
          { type: 'danger', id: '222', message: 'foo' }
        ],
        bookedHotel: null
      };

      const expectedResult: fromApplication.State = {
        alerts: [
          { type: 'danger', id: '111', message: 'foo' },
          { type: 'danger', id: '222', message: 'foo' }
        ],
        bookedHotel: null
      };

      const result = reducer(initialState, new DismissAlertAction('333'));
      expect(result).toEqual(expectedResult);
    });

    it('should return an empty alerts array', () => {
      const initialState: fromApplication.State = {
        alerts: [
          { type: 'danger', id: '111', message: 'foo'},
        ],
        bookedHotel: null
      };

      const expectedResult: fromApplication.State = {
        alerts: [],
        bookedHotel: null
      };

      const result = reducer(initialState, new DismissAlertAction('111'));
      expect(result).toEqual(expectedResult);
    });
  });
});
