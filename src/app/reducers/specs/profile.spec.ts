import { reducer } from '../profile';
import * as fromProfile from '../profile';

import {
  LoadIdentityAction, LoadIdentityFailAction, LoadIdentitySuccessAction
} from '../../actions/profile';

import { PageData } from '../../models/page-data';
import { Identity } from '../../models/identity';

describe('ApplicationReducer', () => {

  describe('undefined action', () => {
    it('should return the default state', () => {
      const action = {} as any;
      const result = reducer(undefined, action);
      expect(result).toEqual(fromProfile.initialState);
    });
  });

  describe('LOAD_PROFILE', () => {
    it('should change nothing', () => {
      const expectedResult: fromProfile.ProfileState = { ...fromProfile.initialState };
      const result = reducer(fromProfile.initialState, new LoadIdentityAction());
      expect(result).toEqual(expectedResult);
    });
  });

  describe('LOAD_PROFILE_SUCCESS', () => {
    it('should change profile by payload', () => {
      const identity = {
        userId: 'foobar',
        name: 'foo',
        loginName: 'foo@bar.de',
      } as Identity;
      const license = null;
      const page = new PageData();
      const expectedResult: fromProfile.ProfileState = {
        profile: identity, page: page
      };
      const result = reducer(fromProfile.initialState, new LoadIdentitySuccessAction({ identity }));
      expect(result).toEqual(expectedResult);
    });
  });

  describe('LOAD_PROFILE_FAIL', () => {
    it('should reset profile and license', () => {
      const profile = {
        userId: 'foobar',
        name: 'foo',
        loginName: 'foo@bar.de'
      };
      
      const page = {
        url: '',
        title: ''
      } as PageData;
      const expectedResult: fromProfile.ProfileState = {
        profile: null, page: page
      };
      const initialState: fromProfile.ProfileState = {
        profile, page: page
      };

      const result = reducer(initialState, new LoadIdentityFailAction());
      expect(result).toEqual(expectedResult);
    });
  });
});
