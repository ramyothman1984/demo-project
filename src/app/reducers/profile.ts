import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PageData } from './../models/page-data';
import { Identity } from '../models/identity';
import * as fromRoot from '.';
import * as profileActions from '../actions/profile';
import { PROFILE_ACTIONS } from '../actions/actions-definition';

export interface ProfileState {
  profile: Identity;
  page: PageData;
}

export const initialState: ProfileState = {
  profile: null,  
  page: new PageData(),
};

export function reducer(state = initialState, action: profileActions.Actions): ProfileState {
  switch (action.type) {
    case PROFILE_ACTIONS.LOAD_IDENTITY_FAIL:
      return { ...state, profile: null };
    case PROFILE_ACTIONS.LOAD_IDENTITY_SUCCESS:
      return loadIdentitySuccess(state, action as profileActions.LoadIdentitySuccessAction);
    case PROFILE_ACTIONS.LOAD_PAGE_DATA:
      return { ...state, page: new PageData() };
    case PROFILE_ACTIONS.LOAD_PAGE_DATA_COMPLETE:
      return { ...state, page: action.payload.page }
    default:
      return state;
  }
}

function loadIdentitySuccess(state: ProfileState, action: profileActions.LoadIdentitySuccessAction) {
  const profile = action.payload.identity;
  return {
    ...state,
    profile
  };
}

export interface AppState extends fromRoot.AppState {
  'profile': ProfileState;
}

export const getProfileState = createFeatureSelector<ProfileState>('profile');
export const getCurrentProfile = createSelector(getProfileState, (state: ProfileState) => state.profile);
export const getPage = createSelector(getProfileState, (state: ProfileState) => state.page);
