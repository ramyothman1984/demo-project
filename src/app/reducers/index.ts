import { ActionReducerMap, createSelector } from '@ngrx/store';

import * as fromApplication from './application';

export interface AppState {
  application: fromApplication.State;
}

export const reducers: ActionReducerMap<AppState> = {
  application: fromApplication.reducer
};

/* STATES */
export const getApplicationState = (state: AppState) => state.application;

/* APPLICATION SELECTORS */
export const getAlerts = createSelector(getApplicationState, fromApplication.getAlerts);
export const getBookedHotel = createSelector(getApplicationState, fromApplication.getBookedHotel);
