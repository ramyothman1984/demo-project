import * as appActions from '../actions/application';

import { Alert } from '../models/alert';
import { APPLICATION_ACTIONS } from '../actions/actions-definition';
import { HotelInfo } from '../models/hotel';

export interface State {
  alerts: Alert[];
  bookedHotel: HotelInfo
}

export const initialState: State = {
  alerts: [],
  bookedHotel: null
};

export function reducer(state = initialState, action: appActions.Actions): State {
  switch (action.type) {
    case APPLICATION_ACTIONS.ADD_ALERT:
      return addAlertReducer(state, action as appActions.AddAlertAction);
    case APPLICATION_ACTIONS.DISMISS_ALERT:
      return dismissReducer(state, action as appActions.DismissAlertAction);
    case APPLICATION_ACTIONS.BOOK_HOTEL:
      return {...state, bookedHotel: {... (action as appActions.BookHotelAction).payload}};
    default:
      return state;
  }
}

function addAlertReducer(state: State, action: appActions.AddAlertAction): State {
  return { ...state, alerts: [...state.alerts, action.payload.alert] };
}

function dismissReducer(state: State, action: appActions.DismissAlertAction): State {
  const index = state.alerts.findIndex(alert => alert.id === action.payload);
  if (index === -1) {
    return state;
  }
  return {
    ...state,
    alerts: [
      ...state.alerts.slice(0, index),
      ...state.alerts.slice(index + 1)
    ]
  };
}

export const getAlerts = (state: State) => state.alerts;
export const getBookedHotel = (state: State) => state.bookedHotel;