export interface Alert {
    id?: string;
    title?: string;
    message: string;
    type: string;
}