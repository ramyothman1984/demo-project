import { GeoLocation } from './geo-location';

export class HotelInfo {
    id: string;
    name: string;
    description: string;
    photo: string;
    location: GeoLocation;
}