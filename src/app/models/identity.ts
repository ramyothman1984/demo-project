export interface Identity {
    userId: string;
    name: string;
    loginName: string;
}
  