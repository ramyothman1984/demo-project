import { Component, OnInit } from '@angular/core';
import { GeoLocation } from 'src/app/models/geo-location';
import * as EventEmitter from 'events';
import { HotelInfo } from 'src/app/models/hotel';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as appActions from '../../actions/application';
import { Router } from '@angular/router';
import { HotelService } from 'src/app/services/hotel.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  geoLocation: GeoLocation;
  hotels: HotelInfo[] = [];
  selectedHotel: HotelInfo;
  positionUpdate = new EventEmitter();
  
  constructor(private storeRoot: Store<fromRoot.AppState>, private router: Router, private hotelService: HotelService) { }

  ngOnInit(): void {
    navigator.geolocation.getCurrentPosition((position) => {
      this.geoLocation = { lat: position.coords.latitude, lng: position.coords.longitude};
      const result = this.hotelService.generateLocations(this.geoLocation);
      this.hotels = result.hotelsInfo;
      this.geoLocation = result.geoLocation;
      this.selectedHotel = result.selectedHotel;
    }, this.positionErrorHandler, {timeout: 2000});
    
  }

  completeBooking(event) {
    this.storeRoot.dispatch(new appActions.BookHotelAction(event));
    this.router.navigate(['confirm-booking']);
  }

  positionErrorHandler(err) {
    // when location isn't available set default location to berlin
    this.geoLocation = {lat: 52.520008, lng: 13.404954};
  }

}
