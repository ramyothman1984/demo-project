import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as appActions from '../../actions/application';
import { Observable } from 'rxjs';
import { HotelInfo } from 'src/app/models/hotel';
@Component({
  selector: 'app-confirm-booking',
  templateUrl: './confirm-booking.component.html',
  styleUrls: ['./confirm-booking.component.scss']
})
export class ConfirmBookingComponent implements OnInit {
  hotel$: Observable<HotelInfo>;
  constructor(private storeRoot: Store<fromRoot.AppState>) { }

  ngOnInit(): void {
    this.hotel$ = this.storeRoot.pipe(select(fromRoot.getBookedHotel));
  }

}
