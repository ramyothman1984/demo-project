import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'limehome';
  constructor(translate: TranslateService, private authService: AuthService) {
    translate.setDefaultLang('en');
    
  }
}
