import { Component, OnInit, Input } from '@angular/core';
import { SlideInOutAnimation } from 'src/app/utils/animation';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss'],
  animations: [SlideInOutAnimation]
})
export class NavigationComponent implements OnInit {
  @Input() show: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
