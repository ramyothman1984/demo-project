import { Component, OnInit, Input, Output } from '@angular/core';
import { HotelInfo } from 'src/app/models/hotel';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hotel-carousal',
  templateUrl: './hotel-carousal.component.html',
  styleUrls: ['./hotel-carousal.component.scss']
})
export class HotelCarousalComponent implements OnInit {
  @Input() hotels: HotelInfo[] = [];
  @Output() booking = new EventEmitter<HotelInfo>();
  constructor() { }

  ngOnInit(): void {

  }

}
