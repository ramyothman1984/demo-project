import { Component, EventEmitter, Input, Output, ChangeDetectionStrategy } from '@angular/core';
import { animate, style, trigger, transition } from '@angular/animations';
import { Alert } from 'src/app/models/alert';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.scss'],
  animations: [
    trigger('alertShown', [
      transition('void => *', [
        style({ opacity: 0 }),
        animate('.2s ease-in', style({ opacity: 1 }))
      ]),
      transition('* => void', [
        style({ opacity: 1 }),
        animate('.2s ease-out', style({ opacity: 0, transform: 'scaleY(0.5)' }))
      ]),
    ]),
  ]
})
export class AlertsComponent {
  @Input() alerts: Alert[];
  @Output() dismissed = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

}
