import { Component, OnInit, Input, ViewChild, ElementRef, ViewChildren, AfterViewInit } from '@angular/core';
import { GeoLocation } from 'src/app/models/geo-location';
import { GeoMapsService } from 'src/app/services/geo-maps.service';
import { BehaviorSubject } from 'rxjs';
import { HotelInfo } from 'src/app/models/hotel';

@Component({
  selector: 'app-hotel-maps',
  templateUrl: './hotel-maps.component.html',
  styleUrls: ['./hotel-maps.component.scss']
})
export class HotelMapsComponent implements OnInit, AfterViewInit {
  @Input() selectedHotel: HotelInfo;
  _location: BehaviorSubject<GeoLocation> = new BehaviorSubject<GeoLocation>(null);
  @Input() set location(value: GeoLocation) {
    if (this.map && value) {
      this.geoMapsService.setCenter(this.map, {... value});
    }
  }
  get location(): GeoLocation {
    return this._location.getValue();
  }
  _mapLocations: BehaviorSubject<HotelInfo[]> = new BehaviorSubject<HotelInfo[]>([])
  @Input() set mapLocations(value: HotelInfo[]) {
    if (this.map && value) {
      
      value.forEach( element => {
        this.geoMapsService.addMarker(this.map, element.location, element.name, element.description,
          this.selectedHotel ? this.selectedHotel.id === element.id ? 
          'https://master.d3gdfobargn84j.amplifyapp.com/assets/img/home-icon-active.svg' :
          'https://master.d3gdfobargn84j.amplifyapp.com/assets/img/home-icon.svg' :
          'https://master.d3gdfobargn84j.amplifyapp.com/assets/img/home-icon.svg');
      });
      this._mapLocations.next(value);
      console.log(this.selectedHotel);
      console.log(this.mapLocations);
    }
  }
  get mapLocations(): HotelInfo[] {
    return this._mapLocations.getValue();
  }
  @ViewChild('hotelMap') hotelMap: ElementRef;
  map: any;
  constructor(private geoMapsService: GeoMapsService) { }

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {
    this.map = this.geoMapsService.initializeMap(this.location, this.hotelMap.nativeElement);
  }

}
