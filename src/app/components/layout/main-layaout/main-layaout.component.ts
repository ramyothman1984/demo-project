import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import * as fromRoot from '../../../reducers';
import * as appActions from '../../../actions/application';
import { Observable, Subscription } from 'rxjs';
@Component({
  selector: 'app-main-layaout',
  templateUrl: './main-layaout.component.html',
  styleUrls: ['./main-layaout.component.scss']
})
export class MainLayaoutComponent implements OnInit {
  showNav: boolean;
  alerts$: Observable<any[]>;
  subscription: Subscription = new Subscription();
  constructor(private storeRoot: Store<fromRoot.AppState>) { }

  ngOnInit(): void {
    this.alerts$ = this.storeRoot.pipe(select(fromRoot.getAlerts));
  }

  dismissAlert(alertId: string) {
    this.storeRoot.dispatch(new appActions.DismissAlertAction(alertId));
  }

}
