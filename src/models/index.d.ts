import { ModelInit, MutableModel, PersistentModelConstructor } from "@aws-amplify/datastore";





export declare class BookingDetails {
  readonly id: string;
  readonly bookingID: string;
  readonly roomID: string;
  readonly from?: string;
  readonly to?: string;
  readonly price?: number;
  constructor(init: ModelInit<BookingDetails>);
  static copyOf(source: BookingDetails, mutator: (draft: MutableModel<BookingDetails>) => MutableModel<BookingDetails> | void): BookingDetails;
}

export declare class Booking {
  readonly id: string;
  readonly hotelsID: string;
  readonly bookingDate?: string;
  readonly userId?: string;
  readonly totalPrice?: string;
  readonly BookingDetails?: (BookingDetails | null)[];
  constructor(init: ModelInit<Booking>);
  static copyOf(source: Booking, mutator: (draft: MutableModel<Booking>) => MutableModel<Booking> | void): Booking;
}

export declare class RoomPrice {
  readonly id: string;
  readonly price?: number;
  readonly priceDate?: string;
  readonly roomID: string;
  constructor(init: ModelInit<RoomPrice>);
  static copyOf(source: RoomPrice, mutator: (draft: MutableModel<RoomPrice>) => MutableModel<RoomPrice> | void): RoomPrice;
}

export declare class Room {
  readonly id: string;
  readonly name?: string;
  readonly size?: string;
  readonly capacity?: string;
  readonly hotelsID: string;
  readonly RoomPrices?: (RoomPrice | null)[];
  readonly totalRooms?: number;
  readonly BookingDetails?: (BookingDetails | null)[];
  constructor(init: ModelInit<Room>);
  static copyOf(source: Room, mutator: (draft: MutableModel<Room>) => MutableModel<Room> | void): Room;
}

export declare class Hotels {
  readonly id: string;
  readonly name?: string;
  readonly phone?: string;
  readonly address?: string;
  readonly long?: string;
  readonly lat?: string;
  readonly Rooms?: (Room | null)[];
  readonly Bookings?: (Booking | null)[];
  constructor(init: ModelInit<Hotels>);
  static copyOf(source: Hotels, mutator: (draft: MutableModel<Hotels>) => MutableModel<Hotels> | void): Hotels;
}