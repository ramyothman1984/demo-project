// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { BookingDetails, Booking, RoomPrice, Room, Hotels } = initSchema(schema);

export {
  BookingDetails,
  Booking,
  RoomPrice,
  Room,
  Hotels
};